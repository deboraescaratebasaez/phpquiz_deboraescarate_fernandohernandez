<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="../css/style.css">
</head>
<body>

    <div class="presentacion">
        <div class="pres__text">
        <h2>Resultado</h2>
        </div>

        <div class="press_post">
        
            <?php

                session_start();
                $nombreJugador ="";
                $respuesta = $_POST['respuestaDiez'];
                array_push($_SESSION['respuestas'],$respuesta);
             

                $arrayRespuestas = $_SESSION['respuestas'];


                // print_r($arrayRespuestas); /para verificar si el array se me completaba con la alternativa o no


                //Crear un arreglo para contabilizar la frecuencia de cada letra
                $frecuenciaLetras = array( 
                    'C' => 0,
                    'P' => 0,
                    'S' => 0,
                    'B' => 0
                );


                for($i=0; $i <10; $i++ ){
                    $res = $arrayRespuestas[$i];
                    $frecuenciaLetras[$res]++; 

                }

                // print_r($frecuenciaLetras); me imprime cuantas letras se repiten


                // Encontrar la letra que más se repite
                $letraMasRepetida = ''; //variable
                $maxRepeticiones = 0;//variable para ver cual letra es la que mas se repite

                //nombre del array as clave => valor de dicha clave 
                foreach ($frecuenciaLetras as $letra => $repeticiones) { 
                    if ($repeticiones > $maxRepeticiones) { //aqui yo verifico la letra actual y el num de repeticiones actuales, asi yo logro ver cual esta siendo mas repetidas y cuantas veces
                        $letraMasRepetida = $letra;
                        $maxRepeticiones = $repeticiones;
                    }
                }

                if(isset($_COOKIE["nombre"])){
                    $nombreJugador = $_COOKIE["nombre"];
                }

                switch ($letraMasRepetida) {
                    case "C":
                        echo $nombreJugador . ", eres Charmander";
                        echo "<img src='../img/CG.gif' alt=''>";
                        $pokemon="Charmander";
                        break;
                    case "P":
                        echo $nombreJugador . ",eres Pikachu";
                        echo "<img src='../img/PG.gif' alt=''>";
                        $pokemon="Pikachu";
                        break;
                    case "S":
                        echo $nombreJugador . ",eres Squirtle";
                        echo "<img src='../img/SG.gif' alt=''>";
                        $pokemon="Squirtle";
                        break;
                    case "B":
                        echo $nombreJugador . ",eres Bulbasaur";
                        echo "<img src='../img/BG.gif' alt=''>";
                        $pokemon="Bulbasaur";
                        break;
                    default:
                        echo "Vuelve a responder";
                }


                session_unset(); // elimina todas las variables de sesion almacenadas en la sesion que se encuentra activa
                session_destroy();


                $ip= "192.168.56.101:3306";
                $database = "QuizPokemon";
                $user = "admin";
                $pass = "admin";

                //establecemos la conexion
                $conexion = mysqli_connect($ip,$user,$pass) or die ("No se ha podido conectar con la base de datos");

                //me posiciono en la base de datos
                mysqli_select_db($conexion, $database) or die ("No existe esa base de datos");


                //Insertar los datos a la base de datos
                $sql = "INSERT INTO Resultados (Nombre, Puntuacion, Resultado, Fecha) VALUES ('$nombreJugador', '$pokemon', '$letraMasRepetida', now())";
                $resultado = mysqli_query($conexion, $sql);



            ?>
            <div class="button">
                <a href="tablero.php">
                <button class="comic-button">VER RESULTADOS</button>
                </a>
            </div>

        </div>

    </div>
</body>
</html>